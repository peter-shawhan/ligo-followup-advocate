# Changelog

## 0.0.29 (unreleased)

-   Updated SPIIR reference to Chichi's thesis.

-   Inserted words "estimated to be" for FAR statement.

-   Added far threshold for pipelines to be included.

## 0.0.28 (2019-04-12)

-   Updated macros to state FAR explicitly; no more capping at 1/century.

## 0.0.27 (2019-04-10)

-   Updated wording in initial circular to match what was sent for the first
    superevent alert sent in O3

-   Updated wording for the RAVEN circulars; particularly when there is a
    combined GW-GRB skymap.

-   Updated to call gstlal as GstLAL when referring to pipelines.

-   Added citations for medium latency GRB-GW search pipelines.

## 0.0.26 (2019-04-09)

-   Updated to call EM-Bright json file from the preferred event files list;
    probabilities are between 0 and 1

## 0.0.25 (2019-04-08)

-   Updated reference for PyCBC Live.

-   Updated wording for the retraction circular.

-   Updated wording for the RAVEN circulars.

-   Updated wording for the medium latency GRB circulars.

-   Added skymap citations for BAYESTAR and LALInference.

## 0.0.24 (2019-03-20)

-   Moved pipeline citations from the circular body to the end and enumerated.

-   Updated the p_astro.json test files to include the mass gap.

-   Synced (but hard-coded) thresholds for sending alerts for CBC and Burst
    superevents.

## 0.0.23 (2019-03-08)

-   Added ability to pull down excluded distance json files created by
    PyGRB and X-pipeline to put into exclusion medium latency GRB circulars.

-   Updated source_classification.json to em_bright.json.

## 0.0.22 (2019-03-07)

-   Updated RAVEN specific circulars to handle sub-threshold GRB triggers.

-   Created medium latency GRB circulars if X-pipeline or PyGRB find a
    coincident GW.

-   Added luminosity distance to the initial circular body when available.

## 0.0.21 (2019-02-27)

-   Un-fixed the "correct" skymap name... i.e., will parse VOEvents for
    skymap_fits param.

-   Updated the code to use the multi-resolution skymap if available.

## 0.0.20 (2019-02-27)

-   Changed LIB to oLIB.

## 0.0.19 (2019-02-27)

-   Added citations for other GW analysis pipelines that find the event other
    than the preferred event pipeline.

-   Fixed how the correct skymap name is deduced.

## 0.0.18 (2019-02-25)

-   Re-worded part of the EM_COINC circulars that gives the time difference
    between the neutrino/GRB trigger and GW candidate event.

## 0.0.17 (2019-02-25)

-   Fixed an unfortunate typo.

## 0.0.16 (2019-02-25)

-   Improved the EM_COINC and initial circulars.

## 0.0.15 (2019-02-22)

-   Added template for EM_COINC circulars, i.e., circulars produced when
    RAVEN finds a coincidence between GW and external triggers.

## 0.0.14 (2019-02-13)

-   Added template for retraction circulars.

## 0.0.13.dev0 (2019-02-13)

-   Update the XPath expression for obtaining the sky map URL to conform to the
    new schema for O3. This should fix the missing sky map paragraphs in
    circulars that are uploaded to GraceDb.

## 0.0.12 (2019-02-12)

-   Circulars include p_astro information when available.

## 0.0.11 (2018-08-02)

-   Circulars are generated with uncertainty ellipse text when they are a good
    approximation.

## 0.0.10 (2018-06-28)

-   Changed how VOEvent text is pulled down; now using `client.files()` method
    versus relying on `client.voevents()` json response.

## 0.0.9 (2018-06-28)

-   Circulars are generated strictly for superevents.

## 0.0.8 (2018-06-27)

-   GraceDb links in circulars now reflect the URL of the GraceDb server from
    which the event originated.

## 0.0.7 (2018-05-24)

-   Drop support for Python 2.

-   Do not print the circular if calling `compose()` from a Python script.

-   Remove duplicate skymaps and keep only the lowest latency ones.

-   Do not include skymaps produced outside the LVC.

-   Optional GraceDb client keyword argument when calling compose().

## 0.0.6 (2018-05-08)

-   Make it easier to call functions from Python scripts, like this:

        >>> from ligo import followup_advocate
        >>> text = followup_advocate.compose('G299232')

-   Add `--service` command line option to set the GraceDB service URL.

## 0.0.5 (2018-05-03)

-   First version released on PyPI.
